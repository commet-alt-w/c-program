#!/bin/sh
## compile c-pgraom

BIN="c-program"

# pre-processor:
#   create pre-processed .i file
gcc -ansi -E "$BIN".c > "$BIN".i

# compiler:
#   create assembly .s file
gcc -ansi -S "$BIN".c

# assembler:
#   create executable machine code .o file
gcc -ansi -c "$BIN".c

if [ -z "$1" ]; then
    # linker:
    #   link together object files from various sources
    #   link function calls with their defintitions
    #   create a.out binary file (or -o program_name file)
    gcc -ansi -Wall "$BIN".c -o "$BIN"
elif [ "$1" == "debug" ]; then
     # re-compile with debug info
     gcc -ansi -Wall -ggdb3 "$BIN".c -o "$BIN"
fi

exit 0
