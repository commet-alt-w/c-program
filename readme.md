# c-program

*an etude in c*

a sample c-program

## requirements

* gcc
* valgrind

## building

* release build
  - `$ cmake -S . -B build/release -DCMAKE_BUILD_TYPE=Release`
  - `$ cmake --build build/release`
* debug build
  - `$ cmake -S . -B build/debug -DCMAKE_BUILD_TYPE=Debug`
  - `$ cmake --build build/debug`

## running

* `$ c-program -1 "qwer rewq"`

## checking for memory leaks

* `$./check-for-memoryleaks.sh build/debug/c-program`
* `$./check-for-memoryleaks.sh build/debug/c-program 2`

