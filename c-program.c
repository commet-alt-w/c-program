/* 
   c-program: an etude in c
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cstruct.h"

#define calc_num_elments_from_bytes(size, type) size/sizeof (type)

/*
   $ ./c-program -1 "asdf fdsa" 
*/
int main(int argc, char **argv) {
  /* hello clang! */
  printf("hello, clang!\n\n");

  /* get command-line arguments */
  const int id = atoi(*(argv+1));
  const char * const argv2ptr = *(argv+2);
  
  /* char is only 1 byte 
     so buffer size is the same as number of elemetnts for char array*/  
  const size_t buffer_size = 4096; 
  char * const nameptr = (char *)calloc(buffer_size, sizeof(char));
  int len_name = 0;
  if (nameptr) {
    len_name = snprintf(nameptr, buffer_size, "%s", argv2ptr);
  }
  
  /* print arguments */
  printf("%d arguments: \n\tname: %s\n\tid: %d\n", argc - 1, nameptr, id);

  /* get format string ready */
  const char format[] = "name: %s\nid: %d\n";
  const char * const formatptr = format;
  
  /* calculate length of final formatted statement */
  const int len = snprintf(NULL, 0, formatptr, nameptr, id);

  /* print vars */
  printf("len: %d\n", len);
  printf("strlen name: %lu\n\n", strlen(nameptr));

  /* initialize custom c datatype pointer */
  const size_t size = len_name * sizeof(char) + sizeof(int) + sizeof(cstruct);
  cstruct * const item1 = (cstruct *)calloc(size, 1);
  strncpy(item1->name, nameptr, len_name);
  item1->id = id;

  /* print some sizes */
  printf("sizeof cstruct: %lu\n", sizeof (cstruct));
  printf("sizeof cstruct *: %lu\n", sizeof (cstruct *));
  printf("sizeof int: %lu\n", sizeof (int));
  printf("sizeof char array: %lu\n", len_name * sizeof(char));
  printf("size of item1: %lu\n", size);

  /* get final result to print */
  char *result = (char *)calloc(len, sizeof(char));
  const int len2 = snprintf(result, len, formatptr, item1->name, item1->id);
  printf("\nlen2: %d\n", len2);
  printf("%s\n", result);

  const int type = STRUCT_TYPE_3;
  const int num_elements = calc_num_elments_from_bytes(buffer_size, unsigned long);
  printf("\nnum_elements: %d\n", num_elements);
  unsigned long * const ids = (unsigned long *)calloc(1, buffer_size);
  if (ids) {
    int i = 0;
    for (i = 0; i < num_elements; i++) {
      *(ids+i) = (unsigned long)i;
    }
  }
  
  const size_t size2 = buffer_size + sizeof(int) + sizeof(cstruct2);
  cstruct2 * const item2 = (cstruct2 *)calloc(1, size2);
  item2->type = type;
  if (item2) {
    memcpy(item2->ids, ids, buffer_size);
  }

  const char format2[] = "\ntype: %d\nids[3]: %lu\n";
  const int len3 = snprintf(NULL, 0, format2, item2->type, *(item2->ids+3));
  char * const result2 = (char *)calloc(len3, sizeof(char));
  const int len4 = snprintf(result2, len3, format2, item2->type, item2->ids[3]);
  printf("len4: %d", len4);
  printf("\n%s\n", result2);
  
  free(result2);
  free(ids);
  free(item2);
  free(result);
  free(item1);
  free(nameptr);
  
  exit(EXIT_SUCCESS);
}
