#!/bin/sh
## check for memory leaks using valgrind
## usage:
##  ./check-for-memoryleaks.sh ./build/debug/c-program

BIN="$1"

echo "$BIN"

PARAM1="-1"
PARAM2="asdf fdsa"

if [ -z "$2" ] || [ "$2" == "1" ]; then
    valgrind --leak-check=full --show-leak-kinds=all \
             --track-origins=yes --verbose \
             "$BIN" "$PARAM1" "$PARAM2"
elif [ "$2" == "2" ]; then
    valgrind --tool=memcheck --leak-check=yes --show-reachable=yes \
             --num-callers=20 --track-fds=yes --track-origins=yes \
             --verbose \
             "$BIN" "$PARAM1" "$PARAM2"
fi
   
exit 0
