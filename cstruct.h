#ifndef C_STRUCT_H
#define C_STRUCT_H

typedef struct {
  int id;
  char name[];
} cstruct;

enum {
  STRUCT_TYPE_1 = 1,
  STRUCT_TYPE_2 = 2,
  STRUCT_TYPE_3 = 3
} cstruct_type;

typedef struct {
  int type;
  unsigned long ids[];
} cstruct2;

#endif
